Ionic proiektuak garatzeko NodeJS, Cordova eta Ionic behar-beharrezkoak dira.

* NodeJS: https://nodejs.org

* npm install -g ionic cordova

* Ionic azken bertsioa: 1.5.5
* Cordova azken bertsioa: 5.0.0

## Beharrezkoa den dokumentazioa ##
* Ionic dokumentazioa: http://ionicframework.com/docs/
* AngularJS: 

## Instalaturik dauden plugin zerrenda: ##

**White List:** cordova plugin add https://github.com/apache/cordova-plugin-whitelist
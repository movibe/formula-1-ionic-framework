/**
 * @ngdoc function
 * @name f1App.controller:MainCtrl
 * @description
 * # DriversCtrl
 * Controller of the f1App
 */
angular.module('f1App.Seasons', [])
  .controller('SeasonsCtrl', function ($scope, seasonsService) {
        seasonsService.getSeasons()
            .then(function(season) {

              //Championship year
          $scope.seasons = season.data.MRData.SeasonTable.Seasons;
        },
        function(error)
        {
        	console.error(error);
        });
 });
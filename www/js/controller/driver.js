/**
 * @ngdoc function
 * @name f1App.controller:DriverCtrl
 * @description
 * # DriverCtrl
 * Controller of the f1App
 */

angular.module('f1App.Driver', [])
  .controller('DriverCtrl', function ($scope, $stateParams, driverService, $ionicLoading, $location, $rootScope) {

    console.log("ID..." + $stateParams.id);

    $ionicLoading.show();
    
    driverService.getDriver($stateParams.id, $stateParams.year)
        .then(function(id) {

          $scope.year = $stateParams.year;
            //Driver details
            $scope.driver = id.data.MRData.StandingsTable.StandingsLists[0].
            		DriverStandings[0];
            //Use to champion season
           	$scope.champion = id.data.MRData.StandingsTable;
             driverService.getDriverRaces($stateParams.id, $stateParams.year)
                .then(function(id) {
                    
                    $scope.races = id.data.MRData.RaceTable.Races;

                    console.log($scope.races[0].Circuit.Location.country);
                    $ionicLoading.hide();
                });
    });

    $scope.goToSource = function (url)
    {
      window.alert(url);
    };  

    $scope.returnToDriversLayout = function ()
    {
      $location.path('app/drivers');
      $rootScope.$apply();
    };
   
    
  });
    

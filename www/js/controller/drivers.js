/**
 * @ngdoc function
 * @name f1App.controller:MainCtrl
 * @description
 * # DriversCtrl
 * Controller of the f1App
 */
angular.module('f1App.Drivers', [])
  .controller('DriversCtrl', function ($scope, driversService, $timeout, $ionicLoading, $location, $rootScope) {
        $scope.loading = true;
        $scope.dataloaad = false;

        $scope.currentyear = new Date().getFullYear();
        $ionicLoading.show();

        driversService.getDrivers()
            .then(function(drivers) {
              $ionicLoading.hide();
              console.log(drivers);

              //Championship year
              $scope.champion = drivers.data
                  .MRData.StandingsTable
                    .StandingsLists[0];
                //Drivers data

              $scope.drivers = drivers.data
                  .MRData.StandingsTable
                    .StandingsLists[0].DriverStandings;

              var i = 0;

              $scope.profile_photos = [];

              for (i = 0; i < $scope.drivers.length; i++)
              {
                $scope.profile_photos.push("img/drivers/" + $scope.drivers[i].Driver.driverId + ".png");
              }
        }),
            function(error) {
              $ionicLoading.hide();
              console.error(error);
      };
      $timeout(function(){ 
            $scope.loading = false;
            $scope.dataload = true;
      },800);

      function imgError(image) {
          image.onerror = "";
          image.src = "/images/ionic.png";
          return true;
      }

    $scope.returnToMainLayout = function ()
    {
      $location.path('app/main');
      $rootScope.$apply();
    };

  });


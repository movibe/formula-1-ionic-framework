

/**
 * @ngdoc function
 * @name f1App.controller:CircuitsCtrl
 * @description
 * # CricuitsCtrl
 * Controller of the f1App
 */
angular.module('f1App.Circuit', [])
  .controller('CircuitCtrl', function($scope, $stateParams, circuitService, $compile, $location, $rootScope) {

  	console.log("ID..." + $stateParams.id);
    circuitService.getCircuitData($stateParams.id)
            .then(function(circuits) {
          //Circuit information
          $scope.circuits= circuits.data.MRData.CircuitTable.Circuits[0];

          ionic.Platform.ready(initialize);
    });

    $scope.returnAllCircuits = function ()
    {
            //Redirect to last edition always to reload new edition
            $location.path('app/circuits');
            $rootScope.$apply();
    };    

    function initialize() {

    //Set default location coordinates
        var myLatlng = new google.maps.LatLng($scope.circuits.Location.lat,$scope.circuits.Location.long);
        
        //Map options
        var mapOptions = {
          center: myLatlng,
          zoom: 5,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        //Load configure in map
        var map = new google.maps.Map(document.getElementById("map"),
            mapOptions);

        var markers_circuit = []; 

        //Add all circuits markers
        
              

              console.log("Name: " + $scope.circuits.circuitName + " / Location: " + $scope.circuits.Location.lat +", "
                                +$scope.circuits.Location.long);

               var marker = new google.maps.Marker({
                  position: new google.maps.LatLng($scope.circuits.Location.lat, $scope.circuits.Location.long),
                  map: map,
                  title: $scope.circuits.circuitName
                });

               markers_circuit.push(marker);

               //Marker + infowindow + angularjs compiled ng-click
              var contentString = "<div><a ng-click='clickTest()'>" + marker.title +"</a></div>";

              var compiled = $compile(contentString)($scope);

              var infowindow = new google.maps.InfoWindow({
                content: compiled[0]
              });

              google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
                console.log("click marker...");
              });

        

        $scope.map = map;
      }
  });

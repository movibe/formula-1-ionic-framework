/**
 * @ngdoc function
 * @name f1App.controller:CircuitsCtrl
 * @description
 * # CricuitsCtrl
 * Controller of the f1App
 */
angular.module('f1App.Circuits', [])
  .controller('CircuitsCtrl', function ($scope, circuitsService, $location, $rootScope) {
        circuitsService.getCircuits()
            .then(function(circuits) {
          //Circuits information
          $scope.circuits = circuits.data.MRData.CircuitTable.Circuits;
        });

        $scope.viewInMap = function ()
        {
        	 //Redirect to last edition always to reload new edition
            $location.path('app/search');
            $rootScope.$apply();
        };

        $scope.returnToMainLayout = function ()
        {
          $location.path('app/main');
            $rootScope.$apply();
        };
  });

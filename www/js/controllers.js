angular.module('f1App.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})
.controller('SearchCtrl', function($scope, $compile, circuitsService) {

  //Start 'initialize' function
  //ionic.Platform.ready(initialize);

  $scope.circuit = [];

  circuitsService.getCircuits()
            .then(function(circuits) {
          //Circuits information
          $scope.circuits = circuits.data.MRData.CircuitTable.Circuits;
          console.log($scope.circuits);

          

          ionic.Platform.ready(initialize);
  }, function(error)
  {
    console.error(error);
    
  });

  function initialize() {

    //Set default location coordinates
        var myLatlng = new google.maps.LatLng(0,0);
        
        //Map options
        var mapOptions = {
          center: myLatlng,
          zoom: 2,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        //Load configure in map
        var map = new google.maps.Map(document.getElementById("map"),
            mapOptions);

        var markers_circuit = []; 

        //Add all circuits markers
        for (var i = 0; i < $scope.circuits.length; i ++)
        {
              $scope.circuit.push({name: $scope.circuits[i].circuitName,
                                          latitude: $scope.circuits[i].Location.lat,
                                          longitude: $scope.circuits[i].Location.long});

              console.log("Name: " + $scope.circuits[i].circuitName + " / Location: " + $scope.circuits[i].Location.lat +", "
                                +$scope.circuits[i].Location.long);

               var marker = new google.maps.Marker({
                  position: new google.maps.LatLng($scope.circuits[i].Location.lat, $scope.circuits[i].Location.long),
                  map: map,
                  title: $scope.circuits[i].circuitName
                });


        }

        
        
        //Marker + infowindow + angularjs compiled ng-click
        
        

        

        $scope.map = map;
      }
});
